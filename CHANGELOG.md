## 2.0.0
* Package can be considered stable and will follow semantic versioning from now on.
* Laravel 5.4 or greater is required from now on.

## 1.0.5
* Added methods getLatestStatus, isLatestStatusIn() to StatusHistoryTrait
* Changed methods scopeLatestStatus(), isLatestStatus() in StatusHistoryTrait

## 1.0.4
* Fixed bug introduced in 1.0.3

## 1.0.3
* Added isLatestStatus() method to StatusHistoryTrait.

## 1.0.2
* Fake meta statuses now copy the payload of the original status in events.

## 1.0.1
* Minor annotation fixes

## 1.0.0
* Initial release