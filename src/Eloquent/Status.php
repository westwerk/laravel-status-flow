<?php

namespace Westwerk\StatusFlow\Eloquent;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Request;
use Westwerk\StatusFlow\Event\PostRevertStatusEvent;
use Westwerk\StatusFlow\Event\PostStatusEvent;
use Westwerk\StatusFlow\Event\PreRevertStatusEvent;
use Westwerk\StatusFlow\Event\PreStatusEvent;
use Westwerk\StatusFlow\Exception\StatusUnknownException;

/**
 * Abstract base class for a status.
 * Extend this class to use it as an status.
 *
 * @package Westwerk\StatusFlow\Eloquent
 * @property integer $id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property string $status
 * @property string|null $ip
 * @property string $user_agent
 * @property integer $weight
 * @property array $payload
 * @property string $comment
 * @property-read string $type
 * @property-read StatusHistoryInterface|Model $entity
 * @method Builder order()
 */
class Status extends Model
{

    /**
     * soft-deleted == reverted status
     */
    use SoftDeletes;

    // status types
    const TYPE_INFO = 'info';
    const TYPE_FLOW = 'flow';
    const TYPE_REVERT = 'revert';
    const TYPE_META = 'meta';
    const TYPE_UNKNOWN = 'unknown';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @var array
     */
    protected $visible = [
        'status',
        'created_at',
    ];

    /**
     * @var array
     */
    protected $casts = [
        'payload' => 'array'
    ];

    /**
     * @var int
     */
    private static $weightCounter = 0;


    /**
     * Status constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        // just pass the attributes to parent and add user agent, ip and weight
        // 'entity' must come first
        uksort($attributes, function ($keyA, $keyB) {
            return $keyA == 'entity' ? ($keyB == 'entity' ? 0 : -1) : 1;
        });
        parent::__construct($attributes + [
                'user_agent' => Request::header('User-Agent'),
                'ip' => Request::ip(),
                'weight' => self::$weightCounter++,
            ]);
    }

    /**
     * @return MorphTo
     */
    public function entity()
    {
        return $this->morphTo();
    }

    /**
     * Query builder with default scopes.
     * @return Builder
     */
    public function newQuery()
    {
        return parent::newQuery()->order();
    }

    /**
     * Scope for ordering latest first.
     *
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrder(Builder $query)
    {
        return $query
            ->orderBy('created_at', 'desc')
            ->orderBy('weight', 'desc');
    }

    /**
     * Setter for the status.
     * Sets the type of the status automatically.
     * @param $status
     * @throws \Exception
     */
    public function setStatusAttribute($status)
    {
        $this->attributes['status'] = $status;
    }

    /**
     * @return string
     */
    public function getTypeAttribute()
    {
        try {
            $type = $this->entity->getTypeOfStatus($this->status);
        } catch (StatusUnknownException $e) {
            $type = self::TYPE_UNKNOWN;
        }
        return $type;
    }

    /**
     * Returns the flow status you get if you add this status to your entity.
     * @return null|string
     */
    public function deduceCurrentFlowStatus()
    {
        switch ($this->type) {
            case self::TYPE_FLOW:
                return $this->status;
            case self::TYPE_REVERT:
                return $this->entity->getPreviousFlowStatus($this->reverts());
            default:
                return null;
        }
    }

    /**
     * Returns whether the given status is implied (through the flow) through this status.
     * @param $status
     * @return bool
     */
    public function hasStatus($status)
    {
        return $this->entity->compareFlowStatuses($status, $this->status) <= 0;
    }

    /**
     * @param StatusHistoryInterface $entity
     * @throws \Exception
     */
    public function setEntityAttribute(StatusHistoryInterface $entity)
    {
        if ($entity instanceof Model) {
            $this->entity()->associate($entity);
        } else {
            throw new \Exception('Entity should be of type ' . Model::class . '.');
        }
    }

    /**
     * Returns the flow status this revert status reverts.
     * Returns null if this status is not a revert status.
     * @return null|string
     */
    public function reverts()
    {
        if ($this->type === self::TYPE_REVERT) {
            return $this->entity->getRevertStatuses()[$this->status];
        }
        return null;
    }

    /**
     * Returns all statuses that the current status reverts, up until the given status, in reverse order.
     * @param $currentStatus
     * @return string[]
     */
    public function getRevertedFlowUntil($currentStatus)
    {
        return array_reverse($this->entity->getStatusFlowFromUntil($this->reverts(), $currentStatus));
    }

    /**
     * Create a pre event instance with this status.
     * @return PreStatusEvent
     */
    public function createPreEvent()
    {
        return new PreStatusEvent($this);
    }

    /**
     * Create a post event instance with this status.
     * @return PostStatusEvent
     */
    public function createPostEvent()
    {
        return new PostStatusEvent($this);
    }

    /**
     * Creates all pre revert events for all statuses that are reverted from the current status.
     * @param $currentStatus
     * @return PreRevertStatusEvent[]
     */
    public function createPreRevertEvents($currentStatus)
    {
        return array_map(function ($reverts) {
            return new PreRevertStatusEvent($this, $reverts);
        }, $this->getRevertedFlowUntil($currentStatus));
    }

    /**
     * Creates all post revert events for all statuses that are reverted from the current status.
     * @param $currentStatus
     * @return PostRevertStatusEvent[]
     */
    public function createPostRevertEvents($currentStatus)
    {
        return array_map(function ($reverts) {
            return new PostRevertStatusEvent($this, $reverts);
        }, $this->getRevertedFlowUntil($currentStatus));
    }

    /**
     * Returns the direct meta statuses of the given status.
     * @return string[]
     */
    public function getDirectMetaStatuses()
    {
        return $this->entity->getDirectMetaStatusesOfStatus($this->status);
    }

    /**
     * Returns the direct and indirect meta statuses of the given status.
     * @return string[]
     */
    public function getAllMetaStatuses()
    {
        return $this->entity->getAllMetaStatusesOfStatus($this->status);
    }

    /**
     * Returns an array of pre status events for each meta status that contains this status directly and indirectly.
     * @return PreStatusEvent[]
     */
    public function createPreMetaStatusEvents()
    {
        $events = [];
        foreach ($this->getDirectMetaStatuses() as $metaStatus) {
            $metaStatus = new FakeStatus([
                'entity' => $this->entity,
                'status' => $metaStatus,
                'payload' => $this->payload,
            ]);
            $events = array_merge($events, $metaStatus->createPreMetaStatusEvents(), [$metaStatus->createPreEvent()]);
        }
        return $events;
    }

    /**
     * Returns an array of post status events for each meta status that contains this status directly and indirectly.
     * @return PreStatusEvent[]
     */
    public function createPostMetaStatusEvents()
    {
        $events = [];
        foreach ($this->getDirectMetaStatuses() as $metaStatus) {
            $metaStatus = new FakeStatus([
                'entity' => $this->entity,
                'status' => $metaStatus,
                'payload' => $this->payload,
            ]);
            $events = array_merge($events, $metaStatus->createPostMetaStatusEvents(), [$metaStatus->createPostEvent()]);
        }
        return $events;
    }
}