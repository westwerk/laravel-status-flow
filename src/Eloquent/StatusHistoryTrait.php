<?php

namespace Westwerk\StatusFlow\Eloquent;

use Event;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Westwerk\StatusFlow\Exception\NotFlowStatusException;
use Westwerk\StatusFlow\Exception\StatusUnknownException;

/**
 * Trait StatusHistoryTrait
 * @package Westwerk\StatusFlow\Eloquent
 * @property-read Status[]|Collection $statusHistory
 * @property-read Status $lastStatus
 * @property-read Status $previousStatus
 * @property-read Status $currentFlowStatus
 * @method static $this status(mixed $statuses, bool $matchOnlyOne = false)
 * @method static $this oneStatus(mixed $statuses)
 * @method static $this allStatuses(mixed $statuses)
 * @method static $this notStatus(mixed $statuses, bool $matchNotAll = false)
 * @method static $this notOneStatus(mixed $statuses)
 * @method static $this notAllStatuses(mixed $statuses)
 * @method static $this latestStatus(mixed $statuses, $limit = null, bool $completeLimit = false)
 */
trait StatusHistoryTrait
{

    /**
     * Returns an array containing the info statuses.
     * @return string[]
     */
    public function getInfoStatuses()
    {
        return [];
    }

    /**
     * Returns an array containing the flow statuses in the right order.
     * The first flow status should be the creation of the entity and cannot be reverted.
     * @return string[]
     */
    public function getFlowStatuses()
    {
        return ['created'];
    }

    /**
     * Returns an array mapping each revert status to one flow status that is reverted by it.
     * When reverting the flow is reset to the status before the reverted one.
     * Never revert the first flow status.
     * @return string[]
     * @internal Revert statuses are not fully implemented yet. Ask Marius to implement it for you :)
     */
    public function getRevertStatuses()
    {
        return [];
    }

    /**
     * Returns an array mapping each meta status to an array of other statuses.
     *
     * Example: ['meta' => ['foo', 'bar']]
     *  -> hasStatus('meta') == hasOneStatus(['foo', 'bar'])
     *  -> addStatus('meta') is not allowed
     *
     * The meta status even can map to other meta status. But you must not create a loop.
     * Example: ['meta' => ['foo', 'bar'], 'super' => ['meta', 'hello']]
     *  -> hasStatus('super') == hasOneStatus(['meta', 'hello']) == hasOneStatus(['foo', 'bar', 'hello'])
     *
     * A meta status always equals to one or more of the statuses it maps to.
     * Example: hasAllStatuses(['meta', 'hello'])
     *  -> true if (('foo' or 'bar' is set) and ('hello' is set))
     *
     * @return string[][]
     */
    public function getMetaStatuses()
    {
        return [];
    }

    /**
     * Relation to the status history.
     * @return MorphMany|Builder|QueryBuilder|Status
     * @throws \Exception
     */
    public function statusHistory()
    {
        if ($this instanceof Model) {
            // the default scope orders the statuses descending, so that the latest statuses come first
            return $this->morphMany(Status::class, 'entity');
        } else {
            throw new \Exception(StatusHistoryTrait::class . ' can only be used by ' . Model::class . '.');
        }
    }

    /**
     * Return single item from relation
     * @return MorphOne|Builder|QueryBuilder|Status
     * @throws \Exception
     */
    private function singleStatus()
    {
        if ($this instanceof Model) {
            return $this->morphOne(Status::class, 'entity');
        } else {
            throw new \Exception(StatusHistoryTrait::class . ' can only be used by ' . Model::class . '.');
        }
    }

    /**
     * @return MorphOne|Builder|QueryBuilder|Status
     * @throws \Exception
     */
    public function lastStatus()
    {
        return $this->singleStatus();
    }

    /**
     * @return MorphOne|Builder|QueryBuilder|Status
     * @throws \Exception
     */
    public function previousStatus()
    {
        $relation = $this->singleStatus();
        $relation->skip(1);
        return $relation;
    }

    /**
     * Returns the current status history. Latest first, oldest last.
     * @return Status[]|Collection
     */
    public function getStatusHistory()
    {
        return $this->statusHistory;
    }

    /**
     * Returns the current flow status
     * @return Status
     */
    public function getCurrentFlowStatusAttribute()
    {
        /** @var Collection $history */
        $history = $this->getStatusHistory()->whereIn('status', $this->getFlowStatuses());
        $current = $history->first();

        return $current ?: new FakeStatus([
            'entity' => $this,
            'status' => $this->getFlowStatuses()[0],
            'created_at' => isset($this->created_at) ? $this->created_at : '0000-01-01 00:00:00',
        ]);
    }

    /**
     * Adds the status to the status history and (if flow status) sets it as current status.
     * @param string $status
     * @param array $data
     * @return null|Status
     * @throws \Exception
     */
    public function addStatus($status, array $data = [])
    {
        if ($this instanceof Model) {

            // create instance
            $status = new Status([
                    'entity' => $this,
                    'status' => $status,
                ] + $data);

            // stop on meta statuses
            if ($status->type == Status::TYPE_META) {
                throw new \Exception(
                    sprintf('%s cannot be added to the status history because it is a meta status.', $status)
                );
            }

            // revert statuses are not tested
            if ($status->type == Status::TYPE_REVERT) {
                throw new \Exception(
                    'Revert statuses are not fully implemented yet. Ask Marius to implement it for you :)'
                );
            }

            // PRE meta status events
            foreach ($status->createPreMetaStatusEvents() as $event) {
                Event::fire($event);
                if ($event->isPropagationStopped()) {
                    return null;
                }
            }

            // PRE event
            $event = $status->createPreEvent();
            Event::fire($event);
            if ($event->isPropagationStopped()) {
                return null;
            }

            // PRE REVERT event
            $currentStatus = $this->currentFlowStatus;
            if ($status->type == Status::TYPE_REVERT) {
                foreach ($status->createPreRevertEvents($currentStatus->status) as $event) {
                    Event::fire($event);
                    if ($event->isPropagationStopped()) {
                        return null;
                    }
                }
            }

            // add to history
            $this->statusHistory()->save($status);

            // update updated_at timestamp
            $this->touch();

            // refresh status history
            $this->load(['statusHistory']);

            // POST REVERT event
            if ($status->type == Status::TYPE_REVERT) {
                foreach ($status->createPostRevertEvents($currentStatus->status) as $event) {
                    Event::fire($event);
                }
            }

            // POST event
            Event::fire($status->createPostEvent());

            // POST meta status events
            foreach ($status->createPostMetaStatusEvents() as $event) {
                Event::fire($event);
            }

            return $status;

        } else {
            throw new \Exception(StatusHistoryTrait::class . ' can only be used by ' . Model::class . '.');
        }
    }

    /**
     * Returns whether this entity has all/one of the given statuses.
     * @param string|array $statuses
     * @param bool $matchOnlyOne
     * @return bool
     */
    public function hasStatus($statuses, $matchOnlyOne = false)
    {
        $statuses = is_array($statuses) ? $statuses : [$statuses];
        if ($matchOnlyOne) {
            return $this->hasOneStatus($statuses);
        }
        return $this->hasAllStatuses($statuses);
    }

    /**
     * Returns whether this entity has one of the given statuses.
     * @param string|array $statuses
     * @return bool
     */
    public function hasOneStatus(array $statuses)
    {
        return array_reduce($statuses, function ($carry, $status) {
            return $carry || $this->hasSingleStatus($status);
        }, false);
    }

    /**
     * Returns whether this entity has all the given statuses.
     * @param string|array $statuses
     * @return bool
     */
    public function hasAllStatuses(array $statuses)
    {
        return array_reduce($statuses, function ($carry, $status) {
            return $carry && $this->hasSingleStatus($status);
        }, true);
    }

    /**
     * Returns whether the given single status is set.
     * @param $status
     * @return bool
     * @throws \Exception
     * @internal Use hasStatus() instead.
     */
    private function hasSingleStatus($status)
    {
        switch ($this->getTypeOfStatus($status)) {

            // flow status: check current flow status
            case Status::TYPE_FLOW:
                return $this->currentFlowStatus->hasStatus($status);

            // meta status: check if one of the referenced statuses is set
            case Status::TYPE_META:
                return $this->hasOneStatus($this->getMetaStatuses()[$status]);

            // info or revert status: simply check if it appears in the status history
            default:
                return $this->hasStatusInHistory($status);

        }
    }

    /**
     * @param string $status
     * @return bool
     * @internal This does not completely check if some status is set. Use hasStatus() instead.
     */
    private function hasStatusInHistory($status)
    {
        return $this->getStatusHistory()->reduce(function ($carry, Status $historyStatus) use ($status) {
            return ($carry || ($historyStatus->status === $status));
        }, false);
    }

    /**
     * Adds the status only if it was not added before.
     * @param $status
     * @param array $data
     * @return null|Status Returns the status when added or null if not added.
     * @throws \Exception
     */
    public function addStatusOnce($status, array $data = [])
    {
        if (!$this->hasStatus($status)) {
            return $this->addStatus($status, $data);
        }
        return null;
    }

    /**
     * Adds the status only if it is the next flow status.
     * @param $status
     * @param array $data
     * @return null|Status Returns the status when added or null if not added.
     * @throws \Exception
     */
    public function addStatusIfNext($status, array $data = [])
    {
        if ($this->isNextFlowStatus($status)) {
            return $this->addStatus($status, $data);
        }
        return null;
    }

    /**
     * Returns the status history, but adds missing flow statuses.
     * This is save()-safe. If you call save() on the entity, the filled in statuses are not persisted.
     * @return Status[]|Collection
     * @throws \Exception
     */
    public function getCompleteStatusHistory()
    {
        $statuses = [];

        // add current flow status if faked
        // this is for cases where the first status is missing
        if (($currentFlowStatus = $this->currentFlowStatus) instanceof FakeStatus) {
            $statuses[] = $currentFlowStatus;
        }

        $lastFlowStatus = null;
        foreach ($this->getStatusHistory()->reverse() as $status) {
            /** @var Status $status */

            // if flow status, fill missing statuses
            if ($status->type === Status::TYPE_FLOW) {

                // missing are all flow statuses between the previous one and this one
                if ($lastFlowStatus) {
                    $missingFlowStatuses = array_slice($this->getStatusFlowFromUntil($lastFlowStatus, $status->status), 1, -1);
                } else {
                    $missingFlowStatuses = array_slice($this->getStatusFlowUntil($status->status), 0, -1);
                }

                // add all missing flow statuses
                foreach ($missingFlowStatuses as $i => $missingFlowStatus) {
                    $statuses[] = new FakeStatus([
                        'entity' => $this,
                        'status' => $missingFlowStatus,
                        // copy most data from the actually existing status
                        'user_agent' => $status->user_agent,
                        'ip' => $status->ip,
                        'created_at' => $status->created_at,
                        'updated_at' => $status->updated_at,
                        'deleted_at' => $status->deleted_at,
                    ]);
                }

                $lastFlowStatus = $status->status;
            }

            // add status
            $statuses[] = $status;
        }

        return Collection::make($statuses)->reverse();
    }

    /**
     * Returns those statuses of the complete status history that are present in the filter array.
     * @param string[] $filter
     * @return Status[]|Collection
     */
    public function getCompleteStatusHistoryFiltered(array $filter)
    {
        return $this->getCompleteStatusHistory()->filter(function (Status $status) use ($filter) {
            return in_array($status->status, $filter);
        });
    }

    /**
     * Returns all info, flow, revert and meta statuses.
     * @return string[]
     */
    public function availableStatuses()
    {
        return array_merge(
            $this->getInfoStatuses(),
            $this->getFlowStatuses(),
            array_keys($this->getRevertStatuses()),
            array_keys($this->getMetaStatuses())
        );
    }

    /**
     * Returns the type of the given status.
     * @param string $status
     * @return string
     * @throws \Exception
     */
    public function getTypeOfStatus($status)
    {
        // check info at last. this makes it easier to handle legacy flow statuses
        if (in_array($status, $this->getFlowStatuses())) {
            return Status::TYPE_FLOW;
        } elseif (array_key_exists($status, $this->getRevertStatuses())) {
            return Status::TYPE_REVERT;
        } elseif (array_key_exists($status, $this->getMetaStatuses())) {
            return Status::TYPE_META;
        } elseif (in_array($status, $this->getInfoStatuses())) {
            return Status::TYPE_INFO;
        } else {
            throw new StatusUnknownException(strtr(
                "':status' is not a valid status, possible statuses are :valid.",
                [
                    ':status' => $status,
                    ':valid' => implode(', ', $this->availableStatuses())
                ]
            ));
        }
    }

    /**
     * Returns the position of the given status in the flow.
     * @param $status
     * @return int
     * @throws \Exception
     */
    public function statusFlowPos($status)
    {
        $flow = $this->getFlowStatuses();
        if (($pos = array_search($status, $flow)) !== false) {
            return $pos;
        }
        throw new NotFlowStatusException(strtr(
            "':status' is not a valid flow status, possible statuses are :valid.",
            [
                ':status' => $status,
                ':valid' => implode(', ', $flow),
            ]
        ));
    }

    /**
     * Returns the flow in the right order until and including the given status.
     * @param $status
     * @return array
     */
    public function getStatusFlowUntil($status)
    {
        return array_slice($this->getFlowStatuses(), 0, $this->statusFlowPos($status) + 1);
    }

    /**
     * Returns the flow in the right order from and including the given status.
     * @param $status
     * @return array
     */
    public function getStatusFlowFrom($status)
    {
        return array_slice($this->getFlowStatuses(), $this->statusFlowPos($status));
    }

    /**
     * Returns the flow in the right order between the given statuses (including).
     * @param $statusFrom
     * @param $statusUntil
     * @return array
     */
    public function getStatusFlowFromUntil($statusFrom, $statusUntil)
    {
        $from = $this->statusFlowPos($statusFrom);
        return array_slice($this->getFlowStatuses(), $from, max(0, $this->statusFlowPos($statusUntil) + 1 - $from));
    }

    /**
     * Compares the statuses by their position in the flow.
     * Returns value of -1 if status1 comes before status2, 1 if vice versa and 0 if those statuses are equal.
     * @param $status1
     * @param $status2
     * @return int
     */
    public function compareFlowStatuses($status1, $status2)
    {
        if ($status1 === $status2) {
            return 0;
        }
        return $this->statusFlowPos($status1) < $this->statusFlowPos($status2) ? -1 : 1;
    }

    /**
     * Returns the status to a given flow position.
     * @param $pos
     * @return string|null
     */
    private function posToFlowStatus($pos)
    {
        $flow = $this->getFlowStatuses();
        return isset($flow[$pos]) ? $flow[$pos] : null;
    }

    /**
     * Returns the status that precedes the given status in the flow order or null if the given one is the first.
     * @param $status
     * @return null|string
     */
    public function getPreviousFlowStatus($status)
    {
        return $this->posToFlowStatus($this->statusFlowPos($status) - 1);
    }

    /**
     * Returns the status that follows the given status in the flow order or null if the given one is the last.
     * @param $status
     * @return null|string
     */
    public function getNextFlowStatus($status)
    {
        return $this->posToFlowStatus($this->statusFlowPos($status) + 1);
    }

    /**
     * Returns whether the given status is the next in the flow.
     * @param string $status
     * @return bool
     */
    public function isNextFlowStatus($status)
    {
        return $this->getNextFlowStatus($this->currentFlowStatus->status) === $status;
    }

    /**
     * Returns whether the given status is the first one in the flow.
     * @param string $status
     * @return bool
     */
    private function isFirstFlowStatus($status)
    {
        return $status === array_first($this->getFlowStatuses());
    }

    /**
     * Applies a scope that matches if one/all of the given statuses match.
     * @param Builder $builder
     * @param string|string[] $statuses
     * @param bool $matchOnlyOne
     * @param callable|null $statusQuery
     * @return Builder
     */
    public function scopeStatus(Builder $builder, $statuses, $matchOnlyOne = false, callable $statusQuery = null)
    {
        $statuses = is_array($statuses) ? $statuses : [$statuses];
        if ($matchOnlyOne) {
            return $this->scopeOneStatus($builder, $statuses, $statusQuery);
        }
        return $this->scopeAllStatuses($builder, $statuses, $statusQuery);
    }

    /**
     * Applies a scope that matches if one or more of the given statuses match.
     * @param Builder $builder
     * @param string[] $statuses
     * @param callable|null $statusQuery
     * @return Builder
     */
    public function scopeOneStatus(Builder $builder, array $statuses, callable $statusQuery = null)
    {
        if (count($statuses)) {
            // make sub query
            $builder->where(function (Builder $builder) use ($statuses, $statusQuery) {
                // connect each status with 'or', since it is ok if only one is true
                foreach ($statuses as $status) {
                    $this->scopeSingleStatus($builder, $status, 'or', $statusQuery);
                }
            });
        } else {
            $builder->getQuery()->whereRaw('false');
        }
        return $builder;
    }

    /**
     * Applies a scope that matches if all the given statuses match.
     * @param Builder $builder
     * @param string[] $statuses
     * @param callable|null $statusQuery
     * @return Builder
     */
    public function scopeAllStatuses(Builder $builder, array $statuses, callable $statusQuery = null)
    {
        if (count($statuses)) {
            // make sub query
            $builder->where(function (Builder $builder) use ($statuses, $statusQuery) {
                // connect each status with 'and', since every status is required
                foreach ($statuses as $status) {
                    $this->scopeSingleStatus($builder, $status, 'and', $statusQuery);
                }
            });
        } else {
            $builder->getQuery()->whereRaw('true');
        }
        return $builder;
    }

    /**
     * Applies a scope that matches if the given status is set.
     * @param Builder $builder
     * @param string $status
     * @param string $boolean
     * @param callable|null $statusQuery
     * @return Builder|StatusHistoryTrait|static
     * @throws \Exception
     * @internal Use scope 'status', 'allStatuses' or 'oneStatus'.
     */
    private function scopeSingleStatus(Builder $builder, $status, $boolean = 'and', callable $statusQuery = null)
    {
        switch ($this->getTypeOfStatus($status)) {

            // flow status
            case Status::TYPE_FLOW:
                return $this->scopeFlowStatus($builder, $status, $boolean, $statusQuery);

            // meta status
            case Status::TYPE_META:
                return $this->scopeMetaStatus($builder, $status, $boolean, $statusQuery);

            // info or revert status
            default:
                return $this->scopeSimpleStatus($builder, $status, $boolean, $statusQuery);

        }
    }

    /**
     * Applies a scope that checks if the given status is in the status history.
     * @param Builder $builder
     * @param string $status
     * @param string $boolean
     * @param callable|null $statusQuery
     * @return Builder|static
     * @internal Use scope 'status', 'allStatuses' or 'oneStatus'.
     */
    private function scopeSimpleStatus(Builder $builder, $status, $boolean = 'and', callable $statusQuery = null)
    {
        $builder->where(function (Builder $builder) use ($status, $statusQuery) {
            return $builder->whereHas('statusHistory', function (Builder $builder) use ($status, $statusQuery) {
                // status must match exactly
                $builder->where('status', $status);
                if ($statusQuery !== null) {
                    $statusQuery($builder);
                }
            });
        }, null, null, $boolean);
        return $builder;
    }

    /**
     * Applies a scope that checks if any of the given or its following flow statuses is present in the status history.
     * @param Builder $builder
     * @param string $status
     * @param string $boolean
     * @param callable|null $statusQuery
     * @return Builder|static
     * @internal Use scope 'status', 'allStatuses' or 'oneStatus'.
     */
    private function scopeFlowStatus(Builder $builder, $status, $boolean = 'and', callable $statusQuery = null)
    {
        // every entity has the first flow
        // if not, it must be faked
        if ($statusQuery === null && $this->isFirstFlowStatus($status)) {
            $builder->getQuery()->whereRaw('true', [], $boolean);
        } else {
            $builder->where(function (Builder $builder) use ($status, $statusQuery) {
                return $builder->whereHas('statusHistory', function (Builder $builder) use ($status, $statusQuery) {
                    // status must match the given flow status or any flow status that comes after it
                    $builder->whereIn('status', $this->getStatusFlowFrom($status));
                    if ($statusQuery !== null) {
                        $statusQuery($builder);
                    }
                });
            }, null, null, $boolean);
        }
        return $builder;
    }

    /**
     * Applies a scope that checks if one of the given meta status' statuses is set.
     * @param Builder $builder
     * @param string $status
     * @param string $boolean
     * @param callable|null $statusQuery
     * @return Builder|static
     * @internal Use scope 'status', 'allStatuses' or 'oneStatus'.
     */
    private function scopeMetaStatus(Builder $builder, $status, $boolean = 'and', callable $statusQuery = null)
    {
        $builder->where(function (Builder $builder) use ($status, $statusQuery) {
            $this->scopeOneStatus($builder, $this->getMetaStatuses()[$status], $statusQuery);
        }, null, null, $boolean);
        return $builder;
    }

    /**
     * Applies a scope that matches if none / not all of the given statuses match.
     * @param Builder $builder
     * @param string|string[] $statuses
     * @param bool $matchNotAll
     * @param callable|null $statusQuery
     * @return Builder
     */
    public function scopeNotStatus(Builder $builder, $statuses, $matchNotAll = false, callable $statusQuery = null)
    {
        $statuses = is_array($statuses) ? $statuses : [$statuses];
        if ($matchNotAll) {
            return $this->scopeNotAllStatuses($builder, $statuses, $statusQuery);
        }
        return $this->scopeNotOneStatus($builder, $statuses, $statusQuery);
    }

    /**
     * Applies a scope that matches if none of the given statuses match.
     * @param Builder $builder
     * @param string[] $statuses
     * @param callable|null $statusQuery
     * @return Builder
     */
    public function scopeNotOneStatus(Builder $builder, array $statuses, callable $statusQuery = null)
    {
        if (count($statuses)) {
            // make sub query
            $builder->where(function (Builder $builder) use ($statuses, $statusQuery) {
                // connect each status with 'and', since all statuses must not be present
                foreach ($statuses as $status) {
                    $this->scopeNotSingleStatus($builder, $status, 'and', $statusQuery);
                }
            });
        } else {
            $builder->getQuery()->whereRaw('true');
        }
        return $builder;
    }

    /**
     * Applies a scope that matches if at least one status does not match.
     * @param Builder $builder
     * @param string[] $statuses
     * @param callable|null $statusQuery
     * @return Builder
     */
    public function scopeNotAllStatuses(Builder $builder, array $statuses, callable $statusQuery = null)
    {
        if (count($statuses)) {
            // make sub query
            $builder->where(function (Builder $builder) use ($statuses, $statusQuery) {
                // connect each status with 'or', since it is ok if at least one status is not present
                foreach ($statuses as $status) {
                    $this->scopeNotSingleStatus($builder, $status, 'or', $statusQuery);
                }
            });
        } else {
            $builder->getQuery()->whereRaw('false');
        }
        return $builder;
    }

    /**
     * Applies a scope that matches if the given status is set.
     * @param Builder $builder
     * @param string $status
     * @param string $boolean
     * @param callable|null $statusQuery
     * @return Builder|StatusHistoryTrait|static
     * @throws \Exception
     * @internal Use scope 'notStatus', 'notAllStatuses' or 'notOneStatus'.
     */
    private function scopeNotSingleStatus(Builder $builder, $status, $boolean = 'and', callable $statusQuery = null)
    {
        switch ($this->getTypeOfStatus($status)) {

            // flow status
            case Status::TYPE_FLOW:
                return $this->scopeNotFlowStatus($builder, $status, $boolean, $statusQuery);

            // meta status
            case Status::TYPE_META:
                return $this->scopeNotMetaStatus($builder, $status, $boolean, $statusQuery);

            // info or revert status
            default:
                return $this->scopeNotSimpleStatus($builder, $status, $boolean, $statusQuery);

        }
    }

    /**
     * Applies a scope that checks if the given status is not in the status history.
     * @param Builder $builder
     * @param string $status
     * @param string $boolean
     * @param callable|null $statusQuery
     * @return Builder|static
     * @internal Use scope 'notStatus', 'notAllStatuses' or 'notOneStatus'.
     */
    private function scopeNotSimpleStatus(Builder $builder, $status, $boolean = 'and', callable $statusQuery = null)
    {
        $builder->where(function (Builder $builder) use ($status, $statusQuery) {
            return $builder->whereHas('statusHistory', function (Builder $builder) use ($status, $statusQuery) {
                // status must not match
                $builder->where('status', $status);
                if ($statusQuery !== null) {
                    $statusQuery($builder);
                }
            }, '=', 0);
        }, null, null, $boolean);
        return $builder;
    }

    /**
     * Applies a scope that checks if all of the given one or its following flow statuses are not present in the status history.
     * @param Builder $builder
     * @param string $status
     * @param string $boolean
     * @param callable|null $statusQuery
     * @return Builder|static
     * @internal Use scope 'notStatus', 'notAllStatuses' or 'notOneStatus'.
     */
    private function scopeNotFlowStatus(Builder $builder, $status, $boolean = 'and', callable $statusQuery = null)
    {
        // every entity has the first flow
        // if not, it must be faked
        $builder->where(function (Builder $builder) use ($status, $statusQuery) {
            if ($statusQuery === null && $this->isFirstFlowStatus($status)) {
                $builder->getQuery()->whereRaw('false');
            } else {
                $builder = $builder->whereHas('statusHistory', function (Builder $builder) use ($status, $statusQuery) {
                    // status must not match the given flow status or any flow status that comes after it
                    $builder->whereIn('status', $this->getStatusFlowFrom($status));
                    if ($statusQuery !== null) {
                        $statusQuery($builder);
                    }
                }, '=', 0);
            }
            return $builder;
        }, null, null, $boolean);
        return $builder;
    }

    /**
     * Applies a scope that checks if not a single one of the meta statuses referred statuses is set.
     * @param Builder $builder
     * @param string $status
     * @param string $boolean
     * @param callable|null $statusQuery
     * @return Builder|static
     * @internal Use scope 'notStatus', 'notAllStatuses' or 'notOneStatus'.
     */
    private function scopeNotMetaStatus(Builder $builder, $status, $boolean = 'and', callable $statusQuery = null)
    {
        $builder->where(function (Builder $builder) use ($status, $statusQuery) {
            $this->scopeNotOneStatus($builder, $this->getMetaStatuses()[$status], $statusQuery);
        }, null, null, $boolean);
        return $builder;
    }

    /**
     * Returns the direct meta statuses of the given status.
     * @param string $status
     * @return string[]
     */
    public function getDirectMetaStatusesOfStatus($status)
    {
        $metaStatuses = [];
        foreach ($this->getMetaStatuses() as $metaStatus => $statuses) {
            if (in_array($status, $statuses)) {
                $metaStatuses[] = $metaStatus;
            }
        }
        return $metaStatuses;
    }

    /**
     * Returns the direct and indirect meta statuses of the given status.
     * The result array is ordered from direct to indirect.
     * @param string $status
     * @return string[]
     */
    public function getAllMetaStatusesOfStatus($status)
    {
        $all = [];
        $queue = [$status];
        while ($status = array_shift($queue)) {
            $metaStatuses = $this->getDirectMetaStatusesOfStatus($status);
            $all = array_merge($all, $metaStatuses);
            $queue = array_merge($queue, $metaStatuses);
        }
        return $all;
    }

    /**
     * Returns the given statuses, but resolves all meta statuses to real statuses.
     * @param array $statuses
     * @return array|string[]
     */
    public function resolveStatuses(array $statuses)
    {
        $resolved = [];

        foreach ($statuses as $key => $status) {
            if ($this->getTypeOfStatus($status) == Status::TYPE_META) {
                $add = $this->resolveStatuses($this->getMetaStatuses()[$status]);
            } else {
                $add = [$key => $status];
            }
            $resolved = array_merge($resolved, $add);
        }

        return $resolved;
    }

    /**
     * Filters entities where the latest status is in $statuses.
     *
     * Optionally $limit can be provided. In that case all entities match ...
     * ... where the latest status when only considering statuses from $limit is in $statuses.
     *
     * If you provide an array as $limit, you have to add $statuses to it, too.
     * Otherwise no entity will match.
     *
     * If $statuses is empty, no entity will match.
     *
     * Example: Entities where the last status is FOO.
     *  Model::latestStatus(FOO)
     *
     * Example: Entities that are STARTED and not followed by ABORTED.
     *  Model::latestStatus(STARTED, [STARTED, ABORTED])
     *  * The entities are allowed to have ABORTED, but if they do, a newer STARTED status must exist.
     *
     * Example: Entities that are either STARTED or STOPPED, but not ABORTED.
     *  Model::latestStatus([STARTED, STOPPED], [STARTED, STOPPED, ABORTED])
     *  * The entities are allowed to have ABORTED, but if they do, a newer STARTED or STOPPED status must exist.
     *
     * @param Builder $builder
     * @param string|array $statuses
     * @param array|null $limit
     * @return Builder
     */
    public function scopeLatestStatus(Builder $builder, $statuses, $limit = null)
    {
        // statuses must be array with numeric keys (is important for resolving meta statuses)
        $statuses = is_array($statuses) ? array_values($statuses) : [$statuses];

        // if no status is given, no entity will match
        if (!count($statuses)) {
            $builder->getQuery()->whereRaw('FALSE');
            return $builder;
        }

        // resolve meta statuses
        $statuses = $this->resolveStatuses($statuses);

        // get this model's table and primary key
        $query = $builder->getQuery();
        $table = $query->from;
        $primaryKey = data_get($this, 'primaryKey', 'id');

        // create sub query for the latest status
        $sub = $this->singleStatus()->getRelated()->newQuery()->applyScopes()->getQuery()
            ->select('status')
            ->from('statuses')
            ->where('entity_type', '=', static::class)
            ->where('entity_id', '=', \DB::raw(sprintf('`%s`.`%s`', $table, $primaryKey)))
            ->limit(1);

        // limit the latest statuses in sub query
        if ($limit !== null) {
            $sub->whereIn('status', $limit);
        }

        // add sub query as having to match the required status
        $query->whereRaw(
            sprintf("(%s) IN (?%s)", $sub->toSql(), str_repeat(',?', count($statuses) - 1)),
            array_merge(
                $sub->getBindings(),
                $statuses
            )
        );

        return $builder;
    }

    /**
     * Returns the latest status.
     * If an array is given as $limit, the latest status among the statuses in $limit is returned.
     *
     * @param null|array|string[] $limit
     * @return null|Status
     */
    public function getLatestStatus($limit = null)
    {
        $history = $limit === null
            ? $this->getCompleteStatusHistory()
            : $this->getCompleteStatusHistoryFiltered($limit);
        return $history->first();
    }

    /**
     * Returns whether the given $status is the latest status.
     * If an array is given as $limit, only those statuses that are in $limit are considered.
     *
     * @param string $status
     * @param null|array|string[] $limit
     * @return bool
     */
    public function isLatestStatus($status, $limit = null)
    {
        return $this->isLatestStatusIn([$status], $limit);
    }

    /**
     * Returns whether the latest status is in $statuses.
     * If an array is given as $limit, only those statuses that are in $limit are considered.
     *
     * @param array|string[] $status
     * @param null|array|string[] $limit
     * @return bool
     */
    public function isLatestStatusIn(array $statuses, $limit = null)
    {
        $latestStatus = $this->getLatestStatus($limit);
        return $latestStatus ? in_array($latestStatus->status, $statuses, true) : false;
    }

}