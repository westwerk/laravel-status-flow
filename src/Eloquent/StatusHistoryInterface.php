<?php

namespace Westwerk\StatusFlow\Eloquent;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;

/**
 * Interface StatusHistoryInterface
 * @package Westwerk\StatusFlow\Eloquent
 */
interface StatusHistoryInterface
{

    /**
     * Returns an array containing the info statuses.
     * @return string[]
     */
    public function getInfoStatuses();

    /**
     * Returns an array containing the flow statuses in the right order.
     * The first flow status should be the creation of the entity and cannot be reverted.
     * @return string[]
     */
    public function getFlowStatuses();

    /**
     * Returns an array mapping each revert status to one flow status that is reverted by it.
     * When reverting the flow is reset to the status before the reverted one.
     * Never revert the first flow status.
     * @return string[]
     * @internal Revert statuses are not fully implemented yet. Ask Marius to implement it for you :)
     */
    public function getRevertStatuses();

    /**
     * Returns an array mapping each meta status to an array of other statuses.
     *
     * Example: ['meta' => ['foo', 'bar']]
     *  -> hasStatus('meta') == hasOneStatus(['foo', 'bar'])
     *  -> addStatus('meta') is not allowed
     *
     * The meta status even can map to other meta status. But you must not create a loop.
     * Example: ['meta' => ['foo', 'bar'], 'super' => ['meta', 'hello']]
     *  -> hasStatus('super') == hasOneStatus(['meta', 'hello']) == hasOneStatus(['foo', 'bar', 'hello'])
     *
     * A meta status always equals to one or more of the statuses it maps to.
     * Example: hasAllStatuses(['meta', 'hello'])
     *  -> true if (('foo' or 'bar' is set) and ('hello' is set))
     *
     * @return string[][]
     */
    public function getMetaStatuses();

    /**
     * @return MorphOne
     */
    public function lastStatus();

    /**
     * @return MorphOne
     */
    public function previousStatus();

    /**
     * Relation to the status history. Latest first, oldest last.
     * @return MorphMany
     */
    public function statusHistory();

    /**
     * Returns the current status history.
     * @return Status[]|Collection
     */
    public function getStatusHistory();

    /**
     * Returns the current flow status
     * @return Status
     */
    public function getCurrentFlowStatusAttribute();

    /**
     * Adds the status to the status history and sets it as current status.
     * @param string $status
     * @param array $data
     * @return null|Status
     */
    public function addStatus($status, array $data = []);

    /**
     * Returns whether this entity has all/one of the given statuses.
     * @param string|array $statuses
     * @param bool $matchOnlyOne
     * @return bool
     */
    public function hasStatus($statuses, $matchOnlyOne = false);

    /**
     * Returns whether this entity has one of the given statuses.
     * @param string|array $statuses
     * @return bool
     */
    public function hasOneStatus(array $statuses);

    /**
     * Returns whether this entity has all the given statuses.
     * @param string|array $statuses
     * @return bool
     */
    public function hasAllStatuses(array $statuses);

    /**
     * Adds the status only if it was not added before.
     * @param $status
     * @param array $data
     * @return null|Status Returns the status when added or null if not added.
     * @throws \Exception
     */
    public function addStatusOnce($status, array $data = []);

    /**
     * Adds the status only if it is the next flow status.
     * @param $status
     * @param array $data
     * @return null|Status Returns the status when added or null if not added.
     * @throws \Exception
     */
    public function addStatusIfNext($status, array $data = []);

    /**
     * Returns the status history, but adds missing flow statuses.
     * This is save()-safe. If you call save() on the entity, the filled in statuses are not persisted.
     * @return Status[]|Collection
     * @throws \Exception
     */
    public function getCompleteStatusHistory();

    /**
     * Returns those statuses of the complete status history that are present in the filter array.
     * @param string[] $filter
     * @return Status[]
     */
    public function getCompleteStatusHistoryFiltered(array $filter);

    /**
     * Returns all info, flow and revert statuses.
     * @return string[]
     */
    public function availableStatuses();

    /**
     * Returns the type of the given status.
     * @param string $status
     * @return string
     * @throws \Exception
     */
    public function getTypeOfStatus($status);

    /**
     * Returns the position of the given status in the flow.
     * @param $status
     * @return int
     * @throws \Exception
     */
    public function statusFlowPos($status);

    /**
     * Returns the flow in the right order until and including the given status.
     * @param $status
     * @return array
     */
    public function getStatusFlowUntil($status);

    /**
     * Returns the flow in the right order from and including the given status.
     * @param $status
     * @return array
     */
    public function getStatusFlowFrom($status);

    /**
     * Returns the flow in the right order between the given statuses (including).
     * @param $statusFrom
     * @param $statusUntil
     * @return array
     */
    public function getStatusFlowFromUntil($statusFrom, $statusUntil);

    /**
     * Compares the statuses by their position in the flow.
     * Returns value of -1 if status1 comes before status2, 1 if vice versa and 0 if those statuses are equal.
     * @param $status1
     * @param $status2
     * @return int
     */
    public function compareFlowStatuses($status1, $status2);

    /**
     * Returns the status that precedes the given status in the flow order or null if the given one is the first.
     * @param $status
     * @return null|string
     */
    public function getPreviousFlowStatus($status);

    /**
     * Returns the status that follows the given status in the flow order or null if the given one is the last.
     * @param $status
     * @return null|string
     */
    public function getNextFlowStatus($status);

    /**
     * Returns whether the given status is the next in the flow.
     * @param string $status
     * @return bool
     */
    public function isNextFlowStatus($status);

    /**
     * Applies a scope that matches if one/all of the given statuses match.
     * @param Builder $builder
     * @param string|string[] $statuses
     * @param bool $matchOnlyOne
     * @return Builder
     */
    public function scopeStatus(Builder $builder, $statuses, $matchOnlyOne = false);

    /**
     * Applies a scope that matches if one or more of the given statuses match.
     * @param Builder $builder
     * @param string[] $statuses
     * @return Builder
     */
    public function scopeOneStatus(Builder $builder, array $statuses);

    /**
     * Applies a scope that matches if all the given statuses match.
     * @param Builder $builder
     * @param string[] $statuses
     * @return Builder
     */
    public function scopeAllStatuses(Builder $builder, array $statuses);

    /**
     * Applies a scope that matches if none / not all of the given statuses match.
     * @param Builder $builder
     * @param string|string[] $statuses
     * @param bool $matchNotAll
     * @return Builder
     */
    public function scopeNotStatus(Builder $builder, $statuses, $matchNotAll = false);

    /**
     * Applies a scope that matches if none of the given statuses match.
     * @param Builder $builder
     * @param string[] $statuses
     * @return Builder
     */
    public function scopeNotOneStatus(Builder $builder, array $statuses);

    /**
     * Applies a scope that matches if not all the given statuses match.
     * @param Builder $builder
     * @param string[] $statuses
     * @return Builder
     */
    public function scopeNotAllStatuses(Builder $builder, array $statuses);

    /**
     * Returns the direct meta statuses of the given status.
     * @param string $status
     * @return string[]
     */
    public function getDirectMetaStatusesOfStatus($status);

    /**
     * Returns the direct and indirect meta statuses of the given status.
     * @param string $status
     * @return string[]
     */
    public function getAllMetaStatusesOfStatus($status);

    /**
     * Returns the given statuses, but resolves all meta statuses to real statuses.
     * @param array $statuses
     * @return array|string[]
     */
    public function resolveStatuses(array $statuses);

    /**
     * Filters entities where the latest status is in $statuses.
     *
     * Optionally $limit can be provided. In that case all entities match ...
     * ... where the latest status when only considering statuses from $limit is in $statuses.
     *
     * If you provide an array as $limit, you have to add $statuses to it, too.
     * Otherwise no entity will match.
     *
     * If $statuses is empty, no entity will match.
     *
     * Example: Entities where the last status is FOO.
     *  Model::latestStatus(FOO)
     *
     * Example: Entities that are STARTED and not followed by ABORTED.
     *  Model::latestStatus(STARTED, [STARTED, ABORTED])
     *  * The entities are allowed to have ABORTED, but if they do, a newer STARTED status must exist.
     *
     * Example: Entities that are either STARTED or STOPPED, but not ABORTED.
     *  Model::latestStatus([STARTED, STOPPED], [STARTED, STOPPED, ABORTED])
     *  * The entities are allowed to have ABORTED, but if they do, a newer STARTED or STOPPED status must exist.
     *
     * @param Builder $builder
     * @param string|array $statuses
     * @param array|null $limit
     * @return Builder
     */
    public function scopeLatestStatus(Builder $builder, $statuses, $limit = null);

    /**
     * Returns the latest status.
     * If an array is given as $limit, the latest status among the statuses in $limit is returned.
     *
     * @param null|array|string[] $limit
     * @return null|Status
     */
    public function getLatestStatus($limit = null);

    /**
     * Returns whether the given $status is the latest status.
     * If an array is given as $limit, only those statuses that are in $limit are considered.
     *
     * @param string $status
     * @param null|array|string[] $limit
     * @return bool
     */
    public function isLatestStatus($status, $limit = null);

    /**
     * Returns whether the latest status is in $statuses.
     * If an array is given as $limit, only those statuses that are in $limit are considered.
     *
     * @param array|string[] $status
     * @param null|array|string[] $limit
     * @return bool
     */
    public function isLatestStatusIn(array $statuses, $limit = null);
}