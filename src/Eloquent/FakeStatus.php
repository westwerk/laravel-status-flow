<?php

namespace Westwerk\StatusFlow\Eloquent;


use Illuminate\Database\Eloquent\Model;

/**
 * The fake status fills in gaps of the flow status.
 * It is only temporary and should not be persisted in any way.
 *
 * @package Westwerk\StatusFlow\Eloquent
 */
class FakeStatus extends Status
{

    /**
     * @var StatusHistoryInterface|Model
     */
    public $entity;

    /**
     * @param StatusHistoryInterface $entity
     * @throws \Exception
     */
    public function setEntityAttribute(StatusHistoryInterface $entity)
    {
        if ($entity instanceof Model) {
            $this->entity = $entity;
        } else {
            throw new \Exception('Entity should be of type ' . Model::class . '.');
        }
    }
}