<?php

namespace Westwerk\StatusFlow;

use Illuminate\Database\Schema\Blueprint;

/**
 * This is a class that is used by migrations.
 * The database structure this class generates must never change. Changing it could break migrations using it.
 * @package Westwerk\StatusFlow
 */
abstract class StatusMigrationHelperV1
{

    /**
     * @var string
     */
    private static $currentFlowStatusColumn = 'current_flow_status_id';

    /**
     * Creates a basic table that the Status model can use.
     * @param Blueprint $table
     */
    public static function createColumns(Blueprint $table)
    {
        // id
        $table->increments('id');

        // model reference
        $table->string($typeCol = 'entity_type');
        $table->unsignedInteger($idCol = 'entity_id');
        $table->index([$typeCol, $idCol], 'entity_index');

        // type identifier
        $table->string('type');

        // status identifier
        $table->string('status');

        // statuses are soft delete
        // yet, through the entity-relation they will still be deleted for real if the entity is deleted
        $table->softDeletes();

        // ip address
        $table->ipAddress('ip')->nullable()->default(null);

        // user agent
        $table->text('user_agent')->default('');

        // payload (json array)
        $table->text('payload');

        // text comment
        $table->text('comment')->default('');

        // created_at and updated_at
        $table->timestamps();

        // weight (used for ordering)
        $table->integer('weight')->default(0);
    }

    /**
     * Creates a current status column for the model that uses the status.
     * @param Blueprint $table
     * @param string $statusTable
     * @param string $statusTableIdColumn
     */
    public static function createCurrentFlowStatusColumn(Blueprint $table, $statusTable, $statusTableIdColumn = 'id')
    {
        $table->unsignedInteger(self::$currentFlowStatusColumn)->nullable()->index();
        $table->foreign(self::$currentFlowStatusColumn)
            ->references($statusTableIdColumn)->on($statusTable)
            // if the current status is deleted, set reference to null
            ->onDelete('set null');
    }

    /**
     * Drops the current status column on the model that uses the status.
     * @param Blueprint $table
     */
    public static function dropCurrentFlowStatusColumn(Blueprint $table)
    {
        $table->dropForeign(strtr('table_column_foreign', [
            'table' => $table->getTable(),
            'column' => self::$currentFlowStatusColumn,
        ]));
        $table->dropColumn(self::$currentFlowStatusColumn);
    }

    /**
     * @return string
     */
    public static function getCurrentFlowStatusColumn()
    {
        return self::$currentFlowStatusColumn;
    }
}