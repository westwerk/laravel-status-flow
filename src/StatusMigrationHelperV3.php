<?php

namespace Westwerk\StatusFlow;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Fluent;

/**
 * This is a class that is used by migrations.
 * The database structure this class generates must never change. Changing it could break migrations using it.
 *
 * This is version 3. Changes since version 2:
 *  * user_agent, payload and comment are nullable with default null
 * @package Westwerk\StatusFlow
 */
abstract class StatusMigrationHelperV3
{

    /**
     * Creates a basic table that the Status model can use.
     * @param Blueprint $table
     */
    public static function createColumns(Blueprint $table)
    {
        // id
        $table->increments('id');

        // model reference
        $table->string($typeCol = 'entity_type');
        $table->unsignedInteger($idCol = 'entity_id');
        $table->index([$typeCol, $idCol], 'entity_index');

        // type identifier
        $table->string('type');

        // status identifier
        $table->string('status');

        // statuses are soft delete
        // yet, through the entity-relation they will still be deleted for real if the entity is deleted
        $table->softDeletes();

        // ip address
        $table->ipAddress('ip')->nullable()->default(null);

        // user agent
        self::columnUserAgent($table);

        // payload (json array)
        self::columnPayload($table);

        // text comment
        self::columnComment($table);

        // created_at and updated_at
        $table->timestamps();

        // weight (used for ordering)
        $table->integer('weight')->default(0);
    }

    /**
     * @param Blueprint $table
     * @return Fluent
     */
    private static function columnUserAgent(Blueprint $table)
    {
        return $table->text('user_agent')->nullable()->default(null);
    }

    /**
     * @param Blueprint $table
     * @return Fluent
     */
    private static function columnPayload(Blueprint $table)
    {
        return $table->text('payload')->nullable()->default(null);
    }

    /**
     * @param Blueprint $table
     * @return Fluent
     */
    private static function columnComment(Blueprint $table)
    {
        return $table->text('comment')->nullable()->default(null);
    }

    /**
     * If you have a V2 status table (or V1 since it is identical), you can use this method to migrate it to V3
     * @param Blueprint $table
     */
    public static function migrateFromV2orV1(Blueprint $table)
    {
        // user agent
        self::columnUserAgent($table)->change();

        // payload (json array)
        self::columnPayload($table)->change();

        // text comment
        self::columnComment($table)->change();
    }

    /**
     * If you want to migrate from V3 to V2 (or V1 since it is identical), you can use this method.
     * @param Blueprint $table
     */
    public static function migrateBackToV2(Blueprint $table)
    {
        // user agent
        self::columnUserAgent($table)->nullable(false)->default('')->change();

        // payload (json array)
        self::columnPayload($table)->nullable(false)->default('')->change();

        // text comment
        self::columnComment($table)->nullable(false)->default('')->change();
    }
}