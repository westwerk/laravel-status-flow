<?php

namespace Westwerk\StatusFlow\Event;

/**
 * This event is thrown before a status is reverted.
 * At the time of this event it is unsure if the status is really reverted.
 * Do not use this event to change things permanently.
 * Use stopPropagation() to prevent the reverting of the status.
 * @package Westwerk\StatusFlow\Event
 */
class PreRevertStatusEvent extends RevertStatusEvent
{
}