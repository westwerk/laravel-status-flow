<?php

namespace Westwerk\StatusFlow\Event;

use Westwerk\StatusFlow\Eloquent\Status;

/**
 * Event base class for reverting statuses.
 * @package Westwerk\StatusFlow\Event
 */
abstract class RevertStatusEvent extends StatusEvent
{

    /**
     * @var string
     */
    public $reverts;

    /**
     * RevertStatusEvent constructor.
     * @param Status $status
     * @param string $reverts
     */
    public function __construct(Status $status, $reverts)
    {
        parent::__construct($status);
        $this->reverts = $reverts;
    }
}