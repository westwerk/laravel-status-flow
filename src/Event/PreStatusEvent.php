<?php

namespace Westwerk\StatusFlow\Event;

/**
 * This event is thrown before a status is applied.
 * At the time of this event it is unsure if the status is really applied.
 * Do not use this event to change things permanently.
 * Use stopPropagation() to prevent the new status from being applied.
 * @package Westwerk\StatusFlow\Event
 */
class PreStatusEvent extends StatusEvent
{
}