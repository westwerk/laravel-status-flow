<?php

namespace Westwerk\StatusFlow\Event;

/**
 * This event is thrown after a new status is applied and some status was reverted.
 * This event is thrown before the PostStatusEvent.
 * @package Westwerk\StatusFlow\Event
 */
class PostRevertStatusEvent extends RevertStatusEvent
{
}