<?php

namespace Westwerk\StatusFlow\Event;

use Symfony\Component\EventDispatcher\Event;
use Westwerk\StatusFlow\Eloquent\Status;

/**
 * Base class for status events.
 * @package Westwerk\StatusFlow\Event
 */
abstract class StatusEvent extends Event
{

    /**
     * @var Status
     */
    public $status;


    /**
     * StatusEvent constructor.
     * @param Status $status
     */
    public function __construct(Status $status)
    {
        $this->status = $status;
    }
}