<?php

namespace Westwerk\StatusFlow\Event;

/**
 * This event is thrown after a new status is applied.
 * This event is thrown after all PostRevertStatusEvent events.
 * @package Westwerk\StatusFlow\Event
 */
class PostStatusEvent extends StatusEvent
{
}