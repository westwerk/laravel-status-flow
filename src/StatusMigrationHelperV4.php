<?php

namespace Westwerk\StatusFlow;

use DB;
use Illuminate\Database\Schema\Blueprint;
use Schema;

/**
 * This is a class that is used by migrations.
 * The database structure this class generates must never change. Changing it could break migrations using it.
 *
 * This is version 4. Changes since version 3:
 *  * type column is removed, it is redundant
 * @package Westwerk\StatusFlow
 */
abstract class StatusMigrationHelperV4
{

    /**
     * Creates a basic table that the Status model can use.
     * @param Blueprint $table
     */
    public static function createColumns(Blueprint $table)
    {
        // id
        $table->increments('id');

        // model reference
        $table->string($typeCol = 'entity_type');
        $table->unsignedInteger($idCol = 'entity_id');
        $table->index([$typeCol, $idCol], 'entity_index');

        // status identifier
        $table->string('status');

        // statuses are soft delete
        // yet, through the entity-relation they will still be deleted for real if the entity is deleted
        $table->softDeletes();

        // ip address
        $table->ipAddress('ip')->nullable()->default(null);

        // user agent
        $table->text('user_agent')->nullable()->default(null);

        // payload (json array)
        $table->text('payload')->nullable()->default(null);

        // text comment
        $table->text('comment')->nullable()->default(null);

        // created_at and updated_at
        $table->timestamps();

        // weight (used for ordering)
        $table->integer('weight')->default(0);
    }

    /**
     * Migrates from V1 and V2 to V4.
     * @param Blueprint $table
     */
    public static function migrateFromV2orV1(Blueprint $table)
    {
        // V1|V2 -> V3
        StatusMigrationHelperV3::migrateFromV2orV1($table);

        // V3 -> V4
        self::migrateFromV3($table);
    }

    /**
     * Migrates from V4 to V2.
     * @param Blueprint $table
     */
    public static function migrateBackToV2(Blueprint $table)
    {
        // V4 -> V3
        self::migrateBackToV3($table);

        // V3 -> V2
        StatusMigrationHelperV3::migrateBackToV2($table);
    }

    /**
     * Migrates from V3 to V4.
     * @param Blueprint $table
     */
    public static function migrateFromV3(Blueprint $table)
    {
        // save old types in payload
        $statusTable = $table->getTable();
        DB::table($statusTable)->orderBy('id')->each(function ($row) use ($statusTable) {

            // load payload
            $payload = json_decode($row->payload, true);
            if (!is_array($payload)) {
                $payload = [];
            }

            // add type to payload
            $payload['_migration_v4_type_rescue'] = $row->type;
            $payload['_migration_v4_payload_rescue'] = ($row->payload !== null);

            // update row with new payload
            $row->payload = json_encode($payload);
            DB::table($statusTable)->where('id', '=', $row->id)->update((array)$row);
        });

        // remove type column
        $table->dropColumn('type');
    }

    /**
     * Migrates from V4 to V3.
     * @param Blueprint $table
     */
    public static function migrateBackToV3(Blueprint $table)
    {
        $statusTable = $table->getTable();

        Schema::table($statusTable, function (Blueprint $table) {
            // type column
            $table->string('type')->after('entity_id');
        });

        // recover old types from payload
        DB::table($statusTable)->orderBy('id')->each(function ($row) use ($statusTable) {

            // load payload
            $payload = json_decode($row->payload, true);
            if (!is_array($payload)) {
                $payload = [];
            }

            // get information saved while migrating from V3 to V4
            $type = array_get($payload, '_migration_v4_type_rescue', 'info');
            $payloadBefore = array_get($payload, '_migration_v4_payload_rescue', true);

            // remove from payload
            array_forget($payload, ['_migration_v4_type_rescue', '_migration_v4_payload_rescue']);

            // update row
            DB::table($statusTable)->where('id', '=', $row->id)->update(array_merge(
                (array)$row,
                [
                    'type' => $type,
                    'payload' => (count($payload) > 0) ? json_encode($payload) : ($payloadBefore ? '' : null),
                ]
            ));
        });
    }
}