<?php

namespace Westwerk\StatusFlow;

use Illuminate\Database\Schema\Blueprint;

/**
 * This is a class that is used by migrations.
 * The database structure this class generates must never change. Changing it could break migrations using it.
 *
 * This is version 2. Changes since version 1:
 *  * current flow status column was removed (the current flow status can be queried through the status history)
 * @package Westwerk\StatusFlow
 */
abstract class StatusMigrationHelperV2
{

    /**
     * Creates a basic table that the Status model can use.
     * @param Blueprint $table
     */
    public static function createColumns(Blueprint $table)
    {
        // id
        $table->increments('id');

        // model reference
        $table->string($typeCol = 'entity_type');
        $table->unsignedInteger($idCol = 'entity_id');
        $table->index([$typeCol, $idCol], 'entity_index');

        // type identifier
        $table->string('type');

        // status identifier
        $table->string('status');

        // statuses are soft delete
        // yet, through the entity-relation they will still be deleted for real if the entity is deleted
        $table->softDeletes();

        // ip address
        $table->ipAddress('ip')->nullable()->default(null);

        // user agent
        $table->text('user_agent')->default('');

        // payload (json array)
        $table->text('payload');

        // text comment
        $table->text('comment')->default('');

        // created_at and updated_at
        $table->timestamps();

        // weight (used for ordering)
        $table->integer('weight')->default(0);
    }
}