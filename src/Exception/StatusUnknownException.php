<?php

namespace Westwerk\StatusFlow\Exception;


class StatusUnknownException extends StatusException
{
}