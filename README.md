# Laravel Status Flow Bundle

Version 1.0.5

## How to enable status flow for an Model `Foobar`

* Let `Foobar` implement `\Westwerk\StatusFlow\Eloquent\StatusHistoryInterface`
    * Use trait `\Westwerk\StatusFlow\Eloquent\StatusHistoryTrait` that implements most of the methods required by the interface.
    * Implement the static methods `getFlowStatuses()`/`getInfoStatuses()`/`getRevertStatuses()` if you have flow/info/revert statuses.
* To add a status: `$foobar->addStatus('some_status')`
    * Where `$foobar` is the instance of `Foobar` that should receive the new status
    * `addStatus()` can take an array as second argument that contains additional data for the status instance:
        * `comment` that can contain any text
        * `payload` for additional data
    * The following statuses will be triggered in that order:
        * `\Westwerk\StatusFlow\Event\PreStatusEvent`
            * If `stopPropagation()` is called on the event, the status will not be persisted.
        * For every reverted status in the reverted flow order: `\Westwerk\StatusFlow\Event\PreRevertStatusEvent`
            * If `stopPropagation()` is called on the event, the status will not be persisted.
        * For every reverted status in the reverted flow order: `\Westwerk\StatusFlow\Event\PostRevertStatusEvent`
        * `\Westwerk\StatusFlow\Event\PostStatusEvent`
    * If propagation was stopped: the status is not persisted and `addStatus()` returns `null`.
    * The status is persisted and returned by `addStatus()`.
* To get the status history: `$foobar->statusHistory`.
* To get the current flow status: `$foobar->currentFlowStatus`.
* To check for a status: `$foobar->hasStatus('some_status')`.
